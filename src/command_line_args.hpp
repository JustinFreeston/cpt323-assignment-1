/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef COMMAND_LINE_ARGS_HPP_
#define COMMAND_LINE_ARGS_HPP_

#include <string>
#include <memory>
#include <iostream>

#include <boost/program_options.hpp>

/**
* @brief Used to parse the command line arguments received from the program
*        into an easily managed class.
*/
class CommandLineArgs {
    boost::program_options::options_description desc;
    boost::program_options::variables_map vm;
    std::unique_ptr<std::string> structure;
    std::unique_ptr<std::string> dictionaryPath;
    std::unique_ptr<std::string> textPath;
    std::unique_ptr<std::string> outputPath;

    public:

    /**
    * @brief Create the CommandLineArgs object with the command line arguments
    *        received by the main function.
    *
    * @param argc The total count of arguments.
    * @param argv The array of character arrays representing the arguments.
    */
    CommandLineArgs(int argc, char** argv);

    /**
    * @brief Checks for the -h or --help command line argument and returns
    *        true if so.
    *
    * @return True if -h or --help command line argument exists. 
    */
    bool isHelp(void);

    /**
    * @brief Print a help message to standard output providing recognized
    *        flags and arguments.
    */
    void printHelp(void);

    /**
    * @brief Checks for all required command line arguments and returns true
    *        if so.
    *
    * @return True if all required arguments exist. 
    */
    bool isRequiredArgs(void);

    /**
    * @brief Get a string reference from the structure argument.
    *
    * @return A string reference from the structure argument. 
    */
    std::string& getStructure(void);

    /**
    * @brief Get a string reference from the dictionary path argument.
    *
    * @return A string reference from the dictionary path argument.
    */
    std::string& getDictionaryPath(void);

    /**
    * @brief Get a string reference from the text path argument.
    *
    * @return A string reference from the text path argument.
    */
    std::string& getTextPath(void);

    /**
    * @brief Get a string reference from the output path argument.
    *
    * @return A string reference from the output path argument.
    */
    std::string& getOutputPath(void);

    /**
    * @brief Load the program_options description with the arguments the program
    *        accept as well as which are required.
    */
    void loadDescription(void);
};

#endif  // COMMAND_LINE_ARGS_HPP_
