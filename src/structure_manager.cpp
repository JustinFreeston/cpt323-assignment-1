/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "structure_manager.hpp"

void StructureManager::initialiseStructure(void) {
    switch (type) {
        case StructureType::LIST:
            structure = std::make_unique<ListStructure>();
            break;
        case StructureType::VECTOR:
            structure = std::make_unique<VectorStructure>();
            break;
        case StructureType::SET:
            structure = std::make_unique<SetStructure>();
            break;
        case StructureType::CUSTOM_LIST:
            structure = std::make_unique<CustomListStructure>();
            break;
        case StructureType::CUSTOM_TREE:
            structure = std::make_unique<CustomTreeStructure>();
            break;
        default:
            throw std::invalid_argument("Structure type is invalid");
    }
}

void StructureManager::loadDictionaryData(const std::string& dictionaryPath) {
    std::ifstream dictionary(dictionaryPath);
    // Path doesn't exist or is not accessable.
    if (!dictionary.good()) {
       throw std::runtime_error("Dictionary path is invalid");
    }

    // Add each line to the dictionary structure.
    std::string line;
    while (std::getline(dictionary, line)) {
        addToDictionary(line);
    }
    dictionary.close();
}

void StructureManager::addToDictionary(const std::string& data) {
    // Ignore blank lines
    if (!data.empty()) {
        // Catch exception that line failed to be added to the structure and
        // continue trying with an appended character. Should probably have a
        // break condition. This is only a problem with a set, but shouldn't
        // happen with the dictionary anyway.
        try {
            structure->insertIntoDictionary(data);
        }
        catch (const std::invalid_argument& e) {
            std::string appended = data;
            appended += APPENDED_CHARACTER;
            // Recursion
            addToDictionary(appended);
        }
    }
}

void StructureManager::loadTextData(const std::string& textPath) {
    std::ifstream text(textPath);
    // Path doesn't exist or is not accessable.
    if (!text.good()) {
        throw std::runtime_error("Text path is invalid");
    }

    // Add each line to the text structure.
    std::string line;
    while (std::getline(text, line)) {
        addToText(line);
    }
    text.close();
}

void StructureManager::addToText(const std::string& data) {
    // Ignore blank lines
    if (!data.empty()) {
        // Catch exception that line failed to be added to the structure and
        // continue trying with an appended character. Should probably have a
        // break condition. This is only a problem with a set.
        try {
            structure->insertIntoText(data);
        }
        catch (const std::invalid_argument& e) {
            std::string appended = data;
            appended += APPENDED_CHARACTER;
            // Recursion
            addToText(appended);
        }
    }
}
