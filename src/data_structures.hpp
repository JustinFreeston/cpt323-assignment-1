/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef DATA_STRUCTURES_HPP_
#define DATA_STRUCTURES_HPP_

#include <string>
#include <iostream>
#include <algorithm>
#include <map>
#include <boost/assign/list_of.hpp>
#include <boost/tokenizer.hpp>

#include "command_line_args.hpp"
#include "structure_manager.hpp"
#include "counting_map.hpp"

// Define a DEBUG macro if DEBUG_FLAG is set
#ifdef DEBUG_FLAG
    #define DEBUG(x) do {std::cerr << x << std::endl; } while (0)
#else
    #define DEBUG(X) do {} while (0)
#endif

const int EXIT_SUCCESSFUL = 0;
const int EXIT_COMMAND_LINE_ERROR = 1;
const int EXIT_STRUCTURE_TYPE_ERROR = 2;
const int EXIT_DICTIONARY_PATH_INVALID = 3;
const int EXIT_TEXT_PATH_INVALID = 4;
const int EXIT_WRITE_OUTPUT_ERROR = 5;

/**
* @brief Get the StructureType from a string representation.
*
* @param structure The string representation to get the StructureType for.
*
* @return StructureType from the string representation structure. 
*/
StructureType toStructureEnum(const std::string& structure);

#endif  // DATA_STRUCTURES_HPP_
