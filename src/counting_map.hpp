/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef COUNTING_MAP_HPP_
#define COUNTING_MAP_HPP_

#include <string>
#include <map>
#include <fstream>

class CountingMap {
    std::map<std::string, int> countMap;
    const char COMMA = ',';

    public:

    /**
    * @brief Increment the value for the key in the member map. If the key does
    *        not exist in the map, it is added and the value is set to 1.
    *
    * @param key The key of the member map to increment.
    */
    inline void increment(const std::string& key) {
        // Increment the value of key.
        countMap[key] += 1;
    }

    /**
    * @brief Get the count for key in the member map.
    *
    * @param key The key of the member map to get the count for.
    *
    * @return The count for the key in member map.
    */
    inline int getCount(const std::string& key) {
        auto val = countMap.find(key);
        if (val != countMap.end()) {
            return val->second;
        }
        else {
            return 0;
        }
    }

    /**
    * @brief Write the key and values of the member map to the file at the path
    *        in CSV format. One key and value is written per line. Throws
    *        runtime_error if the path is invalid.
    *
    * @param path The path to write the file to.
    */
    void writeToFile(const std::string& path);
};

#endif  // COUNTING_MAP_HPP_
