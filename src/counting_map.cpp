/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "counting_map.hpp"

void CountingMap::writeToFile(const std::string& path) {
    // Open a file for writing, truncating existing data.
    std::ofstream out(path, std::ios::trunc | std::ios::out);
    if (!out.good()) {
        throw std::runtime_error("Could not write to file");
    }
    // Write the key and value separated by COMMA to the file.
    for (auto iter = countMap.begin(); iter != countMap.end(); ++iter) {
        out << iter->first << COMMA << iter->second << std::endl;
    }
    out.close();
}
