/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURE_MANAGER_HPP_
#define STRUCTURE_MANAGER_HPP_

#include <string>
#include <memory>
#include <fstream>
#include <stdexcept>

#include "structures/list_structure.hpp"
#include "structures/vector_structure.hpp"
#include "structures/set_structure.hpp"
#include "structures/custom_list_structure.hpp"
#include "structures/custom_tree_structure.hpp"

const char APPENDED_CHARACTER = ' ';

/**
* @brief Valid structure types, and one representing unknown.
*/
enum StructureType {
    LIST,
    VECTOR,
    SET,
    CUSTOM_LIST,
    CUSTOM_TREE,
    UNKNOWN
};

/**
* @brief Manages structures. Loads files into the specified structure type and
*        allows access to the structure instance.
*/
class StructureManager {
    StructureType type;
    std::unique_ptr<IStructure> structure;

    public:

    /**
    * @brief Create a StructureManager object using the StructureType type_. The
    *        structure is also initialised during creation.
    *
    * @param type_ The StructureType that this object will use to load and
    *              retrieve data.
    */
    StructureManager(StructureType type_) {
        type = type_;
        initialiseStructure();
    }

    /**
    * @brief Open the file at the path dictionaryPath and add each line to the
    *        dictionary structure in the member object IStructure. Throws
    *        runtime_error exception if the path is invalid.
    *
    * @param dictionaryPath The path to the dictionary file to be loaded.
    */
    void loadDictionaryData(const std::string& dictionaryPath);

    /**
    * @brief Open the file at the path textPath and add each line to the text
    *        structure in the member object IStructure. Throws runtime_error
    *        exception if the path is invalid.
    *
    * @param textPath The path to the text file to be loaded.
    */
    void loadTextData(const std::string& textPath);

    /**
    * @brief Get a reference to the IStructure created with the StructureType
    *        used at class initialisation. Each StructureType uses a different
    *        class derived from the IStructure interface.
    *
    * @return A reference to the class created for the StructureType at
    *         initialisation. 
    */
    inline IStructure& getStructure(void) {
        return *structure.get();
    }

    private:

    /**
    * @brief Create the member IStructure object with the type of the member
    *        object StructureType.
    */
    void initialiseStructure(void);

    /**
    * @brief Add the string data to the dictionary structure in the member
    *        object IStructure.
    *
    * @param data The data to add to the dictionary structure.
    */
    void addToDictionary(const std::string& data);

    /**
    * @brief Add the string data to the text structure in the member object
    *        IStructure.
    *
    * @param data The data to add to the text structure.
    */
    void addToText(const std::string& data);
};

#endif  // STRUCTURE_MANAGER_HPP_
