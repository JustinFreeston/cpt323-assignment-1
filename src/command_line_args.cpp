/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "command_line_args.hpp"

namespace po = boost::program_options;

CommandLineArgs::CommandLineArgs(int argc, char** argv) {
    loadDescription();
    po::store(po::parse_command_line(argc, argv, desc), vm);
    structure = nullptr;
    dictionaryPath = nullptr;
    textPath = nullptr;
    outputPath = nullptr;
}

bool CommandLineArgs::isHelp(void) {
    if (vm.count("help") || vm.count("h")) {
        return true;
    }
    return false;
}

void CommandLineArgs::printHelp(void) {
    // Would have been nicer to return desc as a string but couldn't figure it
    // out.
    std::cout << desc << std::endl;
}

bool CommandLineArgs::isRequiredArgs(void) {
    // The exception is thrown if not required arguments exist.
    try {
        po::notify(vm);
    }
    catch (po::error& e) {
        return false;
    }
    return true;
}

std::string& CommandLineArgs::getStructure(void) {
    // Set the structure field from the argument.
    if (!structure) {
        structure = std::make_unique<std::string>(vm["structure"]
                .as<std::string>());
    }
    return *structure.get();
}

std::string& CommandLineArgs::getDictionaryPath(void) {
    // Set the dictionaryPath field from the argument.
    if (!dictionaryPath) {
        dictionaryPath = std::make_unique<std::string>(vm["dictionary"]
                .as<std::string>());
    }
    return *dictionaryPath.get();
}

std::string& CommandLineArgs::getTextPath(void) {
    // Set the textPath field from the argument.
    if (!textPath) {
        textPath = std::make_unique<std::string>(vm["text"]
                .as<std::string>());
    }
    return *textPath.get();
}

std::string& CommandLineArgs::getOutputPath(void) {
    // Set the outputPath field from the argument.
    if (!outputPath) {
        outputPath = std::make_unique<std::string>(vm["output"]
                .as<std::string>());
    }
    return *outputPath.get();
}

void CommandLineArgs::loadDescription(void) {
    desc.add_options()
        ("help,h", "Display help")
        ("structure,s", po::value<std::string>()->required(),
            "Data structure storage method "
            "<list/vector/set/custom_list/custom_tree>")
        ("dictionary,d", po::value<std::string>()->required(),
                "Dictionary file")
        ("text,t", po::value<std::string>()->required(), "Text file")
        ("output,o", po::value<std::string>()->required(),
                "Output destination");
}
