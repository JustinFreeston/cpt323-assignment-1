/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "data_structures.hpp"

int main(int argc, char** argv) {
    // TODO Throws error if all required arguments as well as an unknown
    // argument are used.
    CommandLineArgs cla(argc, argv);
    if (cla.isHelp()) {
        cla.printHelp();
        return EXIT_SUCCESSFUL;
    }
    if (!cla.isRequiredArgs()) {
        std::cerr << "Error: Missing required arguments. "
                "-s, -d, -t, -o are all required." << std::endl;
        return EXIT_COMMAND_LINE_ERROR;
    }

    DEBUG("Structure: " << cla.getStructure());
    DEBUG("Dictionary: " << cla.getDictionaryPath());
    DEBUG("Text: " << cla.getTextPath());
    DEBUG("Output: " << cla.getOutputPath());

    // Check that we know the structure type
    StructureType structureType = toStructureEnum(cla.getStructure());
    if (structureType == StructureType::UNKNOWN) {
        std::cerr << "Error: Unknown structure type." << std::endl;
        return EXIT_STRUCTURE_TYPE_ERROR;
    }

    StructureManager manager(structureType);

    // Load the dictionary and check the path exists
    try {
        manager.loadDictionaryData(cla.getDictionaryPath());
        std::cout << "Dictionary loaded." << std::endl;
    }
    catch (const std::runtime_error& e) {
        std::cerr << "Error: dictionary path is invalid." << std::endl;
        return EXIT_DICTIONARY_PATH_INVALID;
    }

    // Load the text and check the path exists
    try {
        manager.loadTextData(cla.getTextPath());
        std::cout << "Text loaded." << std::endl;
    }
    catch (const std::runtime_error& e) {
        std::cerr << "Error: text path is invalid." << std::endl;
        return EXIT_TEXT_PATH_INVALID;
    }

    IStructure& structure = manager.getStructure();

    CountingMap countingMap;

    // https://theboostcpplibraries.com/boost.tokenizer
    using tokenizer = boost::tokenizer<boost::char_separator<char>>;
    boost::char_separator<char> sep{" 1234567890!@#$%^&*()_+=[{}]"
            "\\|;:'\"<>,./?"};

    // Loop through each element in the text structure
    int size = structure.getTextSize();
    for (int i = 0; i < size; ++i) {
        std::string line = structure.textGetNext();
        DEBUG("Line is: \"" << line << "\"");

        // Loop through each token in the line
        tokenizer tok{line, sep};
        for (auto iter = tok.begin(); iter != tok.end(); ++iter) {
            std::string token = *iter;
            // Make sure the token is lower case for matching
            // http://stackoverflow.com
            // /questions/313970/how-to-convert-stdstring-to-lower-case
            std::transform(token.begin(), token.end(), token.begin(),
                    ::tolower);
            DEBUG("Token is: \"" << token << "\"");

            // Check if the token is in the dictionary and increment the count
            if (structure.dictionaryContains(token)) {
                countingMap.increment(token);
                DEBUG("Count for \"" << token << "\" is now " <<
                        countingMap.getCount(token));
            }
        }
    }

    // Write the output to file, checking if successful
    try {
        countingMap.writeToFile(cla.getOutputPath());
        std::cout << "Word count data written to " << cla.getOutputPath() <<
                std::endl;
    }
    catch (const std::runtime_error& e) {
        std::cerr << "Error: Could not write output to file " <<
                cla.getOutputPath() << std::endl;
        return EXIT_WRITE_OUTPUT_ERROR;
    }

    return EXIT_SUCCESSFUL;
}

StructureType toStructureEnum(const std::string& structure) {
    // Map the string representation with the StructureType
    // http://stackoverflow.com/questions/7163069/c-string-to-enum
    std::map<std::string, StructureType> map = boost::assign::map_list_of
        ("list", StructureType::LIST)
        ("vector", StructureType::VECTOR)
        ("set", StructureType::SET)
        ("custom_list", StructureType::CUSTOM_LIST)
        ("custom_tree", StructureType::CUSTOM_TREE);

    // Return the StructureType if it exists
    if (map.count(structure)) {
        return map.find(structure)->second;
    }
    return StructureType::UNKNOWN;
}
