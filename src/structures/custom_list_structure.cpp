/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "custom_list_structure.hpp"

std::string CustomListStructure::textGetNext(void) {
    if (text.empty()) {
        throw std::logic_error("Text is empty");
    }

    if (!text.isPositionSet()) {
        text.resetPosition();
    }
    else {
        try {
            text.incrementPosition();
        }
        // End of the text.
        catch (const std::out_of_range& e) {
            throw std::out_of_range("No more elements");
        }
    }

    return text.getDataAtCurrentPosition();
}
