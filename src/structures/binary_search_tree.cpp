/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "binary_search_tree.hpp"

using BST = BinarySearchTree;

void BST::deleteRoot(void) {
    BST::Node* node = root.get();
    // List is empty.
    if (!node) {
        throw std::logic_error("List is empty");
    }

    if (!node->getLeft()) {
        if (!node->getRight()) {
            // Root is the last node.
            root = nullptr;
        }
        else {
            // Change root to the right branch.
            root = std::move(root->getRightPtr());
        }
    }
    else if (!node->getRight()) {
        // Change root to the left branch.
        root = std::move(root->getLeftPtr());
    }
    else {
        // Change root to the left branch, and add the old right branch to the
        // bottom of the new right branch.
        std::unique_ptr<BST::Node> temp = std::move(root->getRightPtr());
        root = std::move(root->getLeftPtr());

        BST::Node* right = root->getRight();
        if (!right) {
            root->setRight(std::move(temp));
        }
        else {
            while (right->getRight()) {
                right = right->getRight();
            }
            right->setRight(std::move(temp));
        }
    }

    mSize--;
}

void BST::add(std::string data) {
    addRecursion(root.get(), data);
}

bool BST::contains(const std::string& data) {
    return containsRecursion(root.get(), data);
}

void BST::addRecursion(BST::Node* node, const std::string& data) {
    try {
        // Node should only be null if root does not exist.
        if (!node) {
            root = std::make_unique<BST::Node>(data);
            mSize++;
            return;
        }

        // Data should be added to the left branch.
        if (data <= node->getData()) {
            BST::Node* left = node->getLeft();
            if (left) {
                addRecursion(left, data);
            }
            // Add the data.
            else {
                node->setLeft(std::make_unique<BST::Node>(data));
                mSize++;
            }
        }
        // Data should be added to the right branch.
        else {
            BST::Node* right = node->getRight();
            if (right) {
                addRecursion(right, data);
            }
            // Add the data.
            else {
                node->setRight(std::make_unique<BST::Node>(data));
                mSize++;
            }
        }
    }
    // There was a problem allocating memory.
    catch (const std::bad_alloc& e) {
        throw e;
    }
}

bool BST::containsRecursion(BST::Node* node, const std::string& data) {
    if (!node) {
        return false;
    }

    if (node->getData() == data) {
        return true;
    }
    // Continue traversing down the tree.
    else if (node->getData() > data) {
        return containsRecursion(node->getLeft(), data);
    }
    else {
        return containsRecursion(node->getRight(), data);
    }
}
