/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_SET_STRUCTURE_HPP_
#define STRUCTURES_SET_STRUCTURE_HPP_

#include <string>
#include <stdexcept>
#include <set>

#include "structure_interface.hpp"

/**
* @brief Used for adding and accessing dictionary and text sets.
*/
class SetStructure : public IStructure {
    std::set<std::string> dictionary;
    std::set<std::string> text;
    bool positionIteratorSet;
    std::set<std::string>::iterator positionIterator;

    public:

    /**
    * @brief Creates a SetStructure object for adding and accessing dictionary
    * and text sets.
    */
    SetStructure(void) : positionIteratorSet(false) {}

    /**
    * @brief See IStructure.
    */
    inline void insertIntoDictionary(const std::string& data) override {
        if (!dictionary.insert(data).second) {
            throw std::invalid_argument("Duplicate value");
        }
    }

    /**
    * @brief See IStructure.
    */
    inline void insertIntoText(const std::string& data) override {
        if (!text.insert(data).second) {
           throw std::invalid_argument("Duplicate value");
        }
    }

    /**
    * @brief See IStructure.
    */
    inline bool dictionaryContains(const std::string& data) override {
        // Try find data in the dictionary.
        if (dictionary.find(data) != dictionary.end()) {
            return true;
        }
        return false;
    }

    /**
    * @brief See IStructure.
    */
    std::string textGetNext(void) override;

    /**
    * @brief See IStructure.
    */
    inline void textResetPosition(void) override {
        positionIterator = text.begin();
        positionIteratorSet = true;
    }

    /**
    * @brief See IStructure.
    */
    inline int getTextSize(void) override {
        return text.size();
    }
};

#endif  // STRUCTURES_SET_STRUCTURE_HPP_
