/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_VECTOR_STRUCTURE_HPP_
#define STRUCTURES_VECTOR_STRUCTURE_HPP_

#include <string>
#include <vector>
#include <stdexcept>

#include "structure_interface.hpp"

/**
* @brief Used for adding and accessing dictionary and text vectors.
*/
class VectorStructure : public IStructure {
    std::vector<std::string> dictionary;
    std::vector<std::string> text;
    bool positionIteratorSet;
    std::vector<std::string>::iterator positionIterator;

    public:

    /**
    * @brief Creates a VectorStructure object for adding and accessing
    *        dictionary and text vectors.
    */
    VectorStructure(void) : positionIteratorSet(false) {}

    /**
    * @brief See IStructure.
    */
    inline void insertIntoDictionary(const std::string& data) override {
        dictionary.push_back(data);
    }

    /**
    * @brief See IStructure.
    */
    inline void insertIntoText(const std::string& data) override {
        text.push_back(data);
    }

    /**
    * @brief See IStructure.
    */
    bool dictionaryContains(const std::string& data) override;

    /**
    * @brief See IStructure.
    */
    std::string textGetNext(void) override;

    /**
    * @brief See IStructure.
    */
    inline void textResetPosition(void) override {
        positionIterator = text.begin();
        positionIteratorSet = true;
    }

    /**
    * @brief See IStructure.
    */
    inline int getTextSize(void) override {
        return text.size();
    }
};

#endif  // STRUCTURES_VECTOR_STRUCTURE_HPP_
