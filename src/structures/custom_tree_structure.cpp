/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "custom_tree_structure.hpp"

std::string CustomTreeStructure::textGetNext(void) {
    if (text.empty()) {
        throw std::logic_error("Text is empty");
        // Could also be std::out_of_range("No more elements") but can't really
        // track that when deleting the elements.
    }

    std::string data = text.getRootData();
    text.deleteRoot();
    return data;
}
