/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_CUSTOM_TREE_STRUCTURE_HPP_
#define STRUCTURES_CUSTOM_TREE_STRUCTURE_HPP_

#include <string>
#include <stdexcept>

#include "structure_interface.hpp"
#include "binary_search_tree.hpp"

/**
* @brief Used for adding and accessing dictionary and text custom
*        BinarySearchTrees.
*/
class CustomTreeStructure : public IStructure {
    BinarySearchTree dictionary;
    BinarySearchTree text;

    public:

    /**
    * @brief Created a CustomTreeStructure object for adding and accessing
    *        dictionary and text custom BinarySearchTrees.
    */
    CustomTreeStructure(void) {}

    /**
    * @brief See IStructure.
    */
    inline void insertIntoDictionary(const std::string& data) override {
        dictionary.add(data);
    }

    /**
    * @brief See IStructure.
    */
    inline void insertIntoText(const std::string& data) override {
        text.add(data);
    }

    /**
    * @brief See IStructure.
    */
    inline bool dictionaryContains(const std::string& data) override {
        return dictionary.contains(data);
    }

    /**
    * @brief See IStructure.
    */
    std::string textGetNext(void) override;

    /**
    * @brief See IStructure.
    */
    inline void textResetPosition(void) override {
        // Elements are deleted when they are accessed.
        // Not a great solution but it's the one we got.
        return;
    }

    /**
    * @brief See IStructure.
    */
    inline int getTextSize(void) override {
        return text.size();
    }
};

#endif  // STRUCTURES_CUSTOM_TREE_STRUCTURE_HPP_
