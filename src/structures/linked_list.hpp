/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_LINKED_LIST_HPP_
#define STRUCTURES_LINKED_LIST_HPP_

#include <memory>
#include <string>
#include <stdexcept>

/**
* @brief Custom linked list implementation.
*/
class LinkedList {

    /**
    * @brief A Node for the LinkedList that contains data and a pointer to the
    *        next Node.
    */
    class Node {
        std::string data;
        std::unique_ptr<Node> next;

        public:

        /**
        * @brief Create a new Node for the LinkedList with the data newData and
        *        a nullptr next Node.
        *
        * @param newData The data to set for the Node.
        */
        Node(std::string newData) : data(newData), next(nullptr) {}

        /**
        * @brief Set the member next to the unique_ptr newNext.
        *
        * @param newNext The new next.
        */
        inline void setNext(std::unique_ptr<Node>&& newNext) {
            next = std::move(newNext);
        }

        /**
        * @brief Get a pointer to the next Node.
        *
        * @return A pointer to the next Node.
        */
        inline Node* getNext(void) {
            return next.get();
        }

        /**
        * @brief Get a unique_ptr to the next Node.
        *
        * @return  A unique_ptr to the next Node.
        */
        inline std::unique_ptr<Node>& getNextPtr(void) {
            return next;
        }

        /**
        * @brief Get the data stored in the Node.
        *
        * @return A reference to the data stored in the Node.
        */
        inline std::string& getData(void) {
            return data;
        }

        friend class LinkedList;
    };

    std::unique_ptr<Node> head;
    int mSize;
    Node* position;

    public:

    /**
    * @brief Create an empty LinkedList.
    */
    LinkedList(void) : head(nullptr), mSize(0), position(nullptr) {}

    /**
    * @brief Get the data stored in the first Node.
    *
    * @return A reference to the data stored in the first Node.
    */
    inline std::string& front(void) {
        return head.get()->getData();
    }

    /**
    * @brief Add a new Node as the first Node with data.
    *
    * @param data The data to add as the first Node.
    */
    void push_front(std::string data);

    /**
    * @brief Delete the first Node. If more Nodes exist in the list, the second
    *        Node will become the first.
    */
    void pop_front(void);

    /**
    * @brief Add a new Node as the last Node with data.
    *
    * @param data The data to add as the last Node.
    */
    void push_back(std::string data);

    /**
    * @brief Delete the last Node.
    */
    void pop_back(void);

    /**
    * @brief Check whether data exists in the list.
    *
    * @param data The data to check for existence in the list.
    *
    * @return True if data exists in the list.
    */
    bool contains(const std::string& data) const;

    /**
    * @brief Get the size of the list.
    *
    * @return The number of elements in the list.
    */
    inline int size(void) const {
        return mSize;
    }

    /**
    * @brief Get whether the list is empty.
    *
    * @return True if the list does not have any elements.
    */
    inline bool empty(void) const {
        return mSize == 0;
    }

    /**
    * @brief Get whether the position member has been initialised.
    *
    * @return True if the position member has been initialised.
    */
    inline bool isPositionSet(void) {
        // Can position be returned and automatically be bool?
        if (position) {
            return true;
        }
        return false;
    }

    /**
    * @brief Increment the position member value to the next Node.
    */
    inline void incrementPosition(void) {
        if (!position->getNext()) {
            throw std::out_of_range("No more elements");
        }
        position = position->getNext();
    }

    /**
    * @brief Reset the position member to the first Node in the list.
    */
    inline void resetPosition(void) {
        position = head.get();
    }

    /**
    * @brief Get the data at the current position member.
    *
    * @return A reference to the data stored in the Node at the position member.
    */
    inline std::string& getDataAtCurrentPosition(void) {
        // Should have a check for nullptr and out of range.
        return position->getData();
    }
};

#endif  // STRUCTURES_LINKED_LIST_HPP_
