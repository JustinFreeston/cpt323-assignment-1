/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_BINARY_SEARCH_TREE_HPP_
#define STRUCTURES_BINARY_SEARCH_TREE_HPP_

#include <memory>
#include <string>
#include <stdexcept>

/**
* @brief Custom binary search tree implementation.
*/
class BinarySearchTree {

    /**
    * @brief A Node for the BinarySearchTree that contains data and left and
    *        right children.
    */
    class Node {
        std::string data;
        std::unique_ptr<Node> left;
        std::unique_ptr<Node> right;

        public:

        /**
        * @brief Create a new Node for the BinarySearchTree with the data
        *        newData and no children.
        *
        * @param newData The data to set for the Node.
        */
        Node(std::string newData) : data(newData), left(nullptr),
                                    right(nullptr) {}

        /**
        * @brief Set the member left to the unique_ptr newLeft.
        *
        * @param newLeft The new left child.
        */
        inline void setLeft(std::unique_ptr<Node>&& newLeft) {
            left = std::move(newLeft);
        }

        /**
        * @brief Set the member right to the unique_ptr newRight.
        *
        * @param newRight The new right child Node.
        */
        inline void setRight(std::unique_ptr<Node>&& newRight) {
            right = std::move(newRight);
        }

        /**
        * @brief Get a pointer to the left child Node.
        *
        * @return A pointer to the left child Node.
        */
        inline Node* getLeft(void) {
            return left.get();
        }

        /**
        * @brief Get a pointer to the right child Node.
        *
        * @return A pointer to the right child Node.
        */
        inline Node* getRight(void) {
            return right.get();
        }

        /**
        * @brief Get a unique_ptr to the left child Node.
        *
        * @return A unique_ptr to the left child Node.
        */
        inline std::unique_ptr<Node>& getLeftPtr(void) {
            return left;
        }

        /**
        * @brief Get a unique_ptr to the right child Node.
        *
        * @return A unique_ptr to the right child Node.
        */
        inline std::unique_ptr<Node>& getRightPtr(void) {
            return right;
        }

        /**
        * @brief Get the data stored in the Node.
        *
        * @return A reference to the data stored in the Node.
        */
        inline std::string& getData(void) {
            return data;
        }

        friend class BinarySearchTree;
    };

    std::unique_ptr<Node> root;
    int mSize;

    public:

    /**
    * @brief Create an empty BinarySearchTree.
    */
    BinarySearchTree(void) : root(nullptr), mSize(0) {}

    /**
    * @brief Get the data stored in the root Node.
    *
    * @return A reference to the data stored in the root Node.
    */
    inline std::string& getRootData(void) {
        return root.get()->getData();
    }

    /**
    * @brief Delete the root Node. If the root Node has children, then a new
    *        root will be set appropriately.
    */
    void deleteRoot(void);

    /**
    * @brief Add the data to the tree. A new Node is created with the data and
    *        attached to the appropriate branch.
    *
    * @param data The data to add to the tree.
    */
    void add(std::string data);

    /**
    * @brief Check whether data exists in the tree.
    *
    * @param data The data to check for existence in the tree.
    *
    * @return True if data exists in the tree.
    */
    bool contains(const std::string& data);

    /**
    * @brief Get the size of the tree.
    *
    * @return The number of elements in the tree.
    */
    inline int size(void) {
        return mSize;
    }

    /**
    * @brief Get whether the tree is empty.
    *
    * @return True if the tree does not have any elements.
    */
    inline bool empty(void) {
        return mSize < 1;
    }

    private:

    /**
    * @brief Recursion method for adding new data to the tree.
    *
    * @param node The current node being compared with data.
    * @param data The data being compared with node.
    */
    void addRecursion(Node* node, const std::string& data);

    /**
    * @brief Recursion method for checking whether data exists in the tree.
    *
    * @param node The current node being checked if the data it contains is
    *             equal to data.
    * @param data The data being checked for if it equals the data contained in
    *             node.
    *
    * @return True if data is equal to the data in node.
    */
    bool containsRecursion(Node* node, const std::string& data);
};

#endif  // STRUCTURES_BINARY_SEARCH_TREE_HPP_
