/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_CUSTOM_LIST_STRUCTURE_HPP_
#define STRUCTURES_CUSTOM_LIST_STRUCTURE_HPP_

#include <string>
#include <stdexcept>

#include "structure_interface.hpp"
#include "linked_list.hpp"

/**
* @brief Used for adding and accessing dictionary and text custom LinkedLists.
*/
class CustomListStructure : public IStructure {
    LinkedList dictionary;
    LinkedList text;

    public:

    /**
    * @brief Created a CustomListStructure object for adding and accessing
    *        dictionary and text custom LinkedLists.
    */
    CustomListStructure(void) {}

    /**
    * @brief See IStructure.
    */
    inline void insertIntoDictionary(const std::string& data) override {
        dictionary.push_front(data);
    }

    /**
    * @brief See IStructure.
    */
    inline void insertIntoText(const std::string& data) override {
        text.push_front(data);
    }

    /**
    * @brief See IStructure.
    */
    inline bool dictionaryContains(const std::string& data) override {
        return dictionary.contains(data);
    }

    /**
    * @brief See IStructure.
    */
    std::string textGetNext(void) override;

    /**
    * @brief See IStructure.
    */
    inline void textResetPosition(void) override {
        text.resetPosition();
    }

    /**
    * @brief See IStructure.
    */
    inline int getTextSize(void) override {
        return text.size();
    }
};

#endif  // STRUCTURES_CUSTOM_LIST_STRUCTURE_HPP_
