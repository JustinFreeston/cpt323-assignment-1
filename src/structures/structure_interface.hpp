/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#ifndef STRUCTURES_STRUCTURE_INTERFACE_HPP_
#define STRUCTURES_STRUCTURE_INTERFACE_HPP_

#include <string>

class IStructure {
    public:
    virtual ~IStructure(void) {}

    /**
    * @brief Insert the data into the dictionary structure.
    *
    * @param data The data to insert into the dictionary structure.
    */
    virtual void insertIntoDictionary(const std::string& data) = 0;

    /**
    * @brief Insert the data into the text structure.
    *
    * @param data The data to insert into the text structure.
    */
    virtual void insertIntoText(const std::string& data) = 0;

    /**
    * @brief Return true if the dictionary structure contains the data.
    *
    * @param data The data to check contained in the dictionary structure.
    *
    * @return True if the dictionary structure contains the data.
    */
    virtual bool dictionaryContains(const std::string& data) = 0;

    /**
    * @brief Get the next string in the text structure.
    *
    * @return The next string in the text structure.
    */
    virtual std::string textGetNext(void) = 0;

    /**
    * @brief Reset the position of the iterator used to track the current
    *        position in the text structure.
    */
    virtual void textResetPosition(void) = 0;

    /**
    * @brief Get the number of elements in the text structure.
    *
    * @return The number of elements in the text structure. 
    */
    virtual int getTextSize(void) = 0;
};

#endif  // STRUCTURES_STRUCTURE_INTERFACE_HPP_
