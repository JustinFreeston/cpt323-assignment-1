/**
 * Student Name:  Justin Freeston
 * Student ID:    s3547990
 * Student Email: s3547990@student.rmit.edu.au
 * 
 * Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
 * Lecturer: Paul Miller (paul.miller@rmit.edu.au)
 * Material: Assignment 1 - Data Structures
 **/

#include "linked_list.hpp"

void LinkedList::push_front(std::string data) {
    auto newNode = std::make_unique<LinkedList::Node>(data);

    try {
        if (head) {
            newNode->setNext(std::move(head));
        }
        head = std::move(newNode);
        mSize++;
    }
    // Throw exception if cannot allocate memory.
    catch (const std::bad_alloc& e) {
        throw e;
    }
}

void LinkedList::pop_front(void) {
    if (!head) {
        throw std::logic_error("List is empty");
    }

    if (!head->getNext()) {
        head = nullptr;
    }
    else {
        head = std::move(head->getNextPtr());
    }

    mSize--;
}

void LinkedList::push_back(std::string data) {
    auto newNode = std::make_unique<LinkedList::Node>(data);

    try {
        if (!head) {
            head = std::move(newNode);
        }
        else {
            auto last = head.get();
            while (last->getNext()) {
                last = last->getNext();
            }
            last->setNext(std::move(newNode));
        }
        mSize++;
    }
    // Throw exception if cannot allocate memory.
    catch (const std::bad_alloc& e) {
        throw e;
    }
}

void LinkedList::pop_back(void) {
    if (!head) {
        throw std::logic_error("List is empty");
    }

    // Loop through the list until second last node.
    auto secondLast = head.get();
    while (secondLast->getNext()->getNext()) {
        secondLast = secondLast->getNext();
    }
    // Cut off the last node.
    secondLast->setNext(nullptr);
    mSize--;
}

bool LinkedList::contains(const std::string& data) const {
    auto current = head.get();
    while (current) {
        if (current->getData() == data) {
            return true;
        }
        current = current->getNext();
    }
    return false;
}
