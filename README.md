```
Student Name:  Justin Freeston
Student ID:    s3547990
Student Email: s3547990@student.rmit.edu.au

Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
Lecturer: Paul Miller (paul.miller@rmit.edu.au)
Material: Assignment 1 - Data Structures
```
Git repo available at https://bitbucket.org/JustinFreeston/cpt323-assignment-1/


Data Structures
======
Data Structures is a program that writes a file containing the number of times
each word in a specified dictionary file appear in a specified text file.
The dictionary file is a plain text file that contains a single word on each
line.
The text file is a plain text file that should preferably not have extremely
long single lines.
The output file is a CSV file that contains words from the dictionary, and how
many times they appear in the text file. The first column is the word, and the
second column is the count. Words that are in the dictionary, but not in the
text file are not included in the output file.

Compiling
------
A simple compile script has been provided in the scripts/ directory. Read the
"Scripts" section in this document for more information about included scripts.

Note: when compiling on the RMIT systems, the g++ command must be sourced by
using `source /opt/rh/devtools-6/enable`.

The Makefile provides three main targets: all, debug, debug-verbose.

* **all**: 
Called by default when typing "make" when in the same directory as the
Makefile. This target is the primary binary for standard use.
* **debug**: 
The program is compiled with the -g flag, for use with debuggers such
as valgrind.
* **debug_verbose**: 
The program is compiled with the -g flag (same as debug target),
and also adds a #define to the source files to provide additional output for
easier debugging and tracing.

Program usage
------
When calling the program from the command line, four arguments are required.
The list below shows the arguments, and their full word equivalent that can
be used instead.

* **-s (--structure)**: 
Must be `list/vector/set/custom_list/custom_tree`.
* **-d (--dictionary)**: 
The path to the dictionary file. Can be a relative path, or absolute path.
* **-t (--text)**: 
The path to the text file. Can be a relative path, or absolute path.
* **-o (--output)**: 
The path to where the output file destination. Will automatically overwrite
if the file already exists. Program must have write permission to destination.
Directories should exist beforehand. Can be a relative path, or absolute path.

Scripts
------
A number of scripts are included in the scripts/ directory. These scripts are
for convenience, and are not required. All scripts expect files to be located
in the parent directory.

* **exec-test.sh**: 
Runs a test on all five data structures and checks the output file is the same
for each. Expects the dictionary "dict.dat" and the text files to be in the
parent directory. By default the text file used is "kaddath.txt" and output
files are created in the parent directory output/ folder. The "kjv.txt" and
"big.txt" files can also be tested by adding the **all** argument when calling
the script. A summary of the test result is output upon completion.
* **make.sh**: 
Used to make the program. Automatically clears the console and sources the
required commands. An additional argument can be provided when calling the
script to use different make targets.
* **run.sh**: 
Run the program with default flags. Uses the list structure, a dictionary file
"dict.dat" located in the parent directory, a text file called "kaddath.txt"
also located in the parent directory, and an output file called
"out-list-kaddath.txt" created in the parent directory folder output/. The type
of structure can be changed to one of the five structures available to the
program using an argument. The text file can also be changed by providing a
second argument called the file name. This file should be located in the parent
directory.
* **run-valgrind.sh**: 
Run the program through the debugger valgrind. The program must be compiled
as debug or debug-verbose (see compiling). The typeo f structure can be changed
to one of the five structures available to the program using an argument. The
text file can also be changed by providing a second argument called the file
name. This file should be located in the parent directory.

