PROGRAM=DataStructures
PROGRAM_DEBUG=$(PROGRAM)-debug
PROGRAM_DEBUG_VERBOSE=$(PROGRAM_DEBUG)-verbose

CC=g++
LINK_FLAGS=-lboost_program_options
FLAGS=-Wall -pedantic -std=c++14
FLAGS_DEBUG=-g
FLAGS_DEBUG_VERBOSE=-DDEBUG_FLAG

SOURCE_DIRECTORY=src/
OBJECT_DIRECTORY=obj/
STRUCTURE_DIRECTORY=structures/
SCRIPTS_DIRECTORY=scripts/

SOURCE_FILES=data_structures.cpp command_line_args.cpp structure_manager.cpp counting_map.cpp structures/linked_list.cpp structures/binary_search_tree.cpp structures/list_structure.cpp structures/vector_structure.cpp structures/set_structure.cpp structures/custom_list_structure.cpp structures/custom_tree_structure.cpp

SOURCES=$(addprefix $(SOURCE_DIRECTORY),$(SOURCE_FILES))
#$(info $$SOURCES is [${SOURCES}])
OBJECTS=$(patsubst %.cpp,$(OBJECT_DIRECTORY)%.o,$(SOURCE_FILES))
#$(info $$OBJECTS is [${OBJECTS}])

.PHONY: all
all: clean_debug clean_debug_verbose compile 
	$(CC) $(LINK_FLAGS) -o $(PROGRAM) $(OBJECTS)

.PHONY: debug
debug: clean_all clean_debug_verbose compile_debug
	$(CC) $(LINK_FLAGS) -o $(PROGRAM_DEBUG) $(OBJECTS)

.PHONY: debug_verbose
debug_verbose: clean_all clean_debug compile_debug_verbose
	$(CC) $(LINK_FLAGS) -o $(PROGRAM_DEBUG_VERBOSE) $(OBJECTS)

compile_debug: FLAGS += $(FLAGS_DEBUG)
compile_debug: compile

compile_debug_verbose: FLAGS += $(FLAGS_DEBUG) $(FLAGS_DEBUG_VERBOSE)
compile_debug_verbose: compile

clean_all:
ifneq ("$(wildcard $(PROGRAM))", "")
clean_all: clean
endif	

clean_debug:
ifneq ("$(wildcard $(PROGRAM_DEBUG))", "")
clean_debug: clean
endif

clean_debug_verbose:
ifneq ("$(wildcard $(PROGRAM_DEBUG_VERBOSE))", "")
clean_debug_verbose: clean
endif

compile: create_dirs $(OBJECTS)

create_dirs:
	mkdir -p $(OBJECT_DIRECTORY)$(STRUCTURE_DIRECTORY)

.PHONY: clean
clean:
	rm -f $(PROGRAM) $(PROGRAM_DEBUG) $(PROGRAM_DEBUG_VERBOSE)
	rm -r $(OBJECT_DIRECTORY)

.PHONY: archive
archive:
	zip $(USER)-A1-$(PROGRAM) -r $(SOURCE_DIRECTORY) $(SCRIPTS_DIRECTORY) Makefile

vpath %.cpp $(SOURCE_DIRECTORY)
vpath %.hpp $(SOURCE_DIRECTORY)

$(OBJECT_DIRECTORY)data_structures.o: data_structures.cpp data_structures.hpp command_line_args.hpp structure_manager.hpp counting_map.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)command_line_args.o: command_line_args.cpp command_line_args.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structure_manager.o: structure_manager.cpp structure_manager.hpp structures/list_structure.hpp structures/vector_structure.hpp structures/set_structure.hpp structures/custom_list_structure.hpp structures/custom_tree_structure.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)counting_map.o: counting_map.cpp counting_map.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/linked_list.o: structures/linked_list.cpp structures/linked_list.hpp structures/structure_interface.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/binary_search_tree.o: structures/binary_search_tree.cpp structures/binary_search_tree.hpp structures/structure_interface.hpp
	$(CC) $(FLAGS) -c $< -o $@ 

$(OBJECT_DIRECTORY)structures/list_structure.o: structures/list_structure.cpp structures/list_structure.hpp structures/structure_interface.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/vector_structure.o: structures/vector_structure.cpp structures/vector_structure.hpp structures/structure_interface.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/set_structure.o: structures/set_structure.cpp structures/set_structure.hpp structures/structure_interface.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/custom_list_structure.o: structures/custom_list_structure.cpp structures/custom_list_structure.hpp structures/structure_interface.hpp structures/linked_list.hpp
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)structures/custom_tree_structure.o: structures/custom_tree_structure.cpp structures/custom_tree_structure.hpp structures/structure_interface.hpp structures/binary_search_tree.hpp
	$(CC) $(FLAGS) -c $< -o $@
