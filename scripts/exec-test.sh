#!/bin/bash

PROGRAM_RELATIVE_DIRECTORY="../"
SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"
ABSOLUTE_DIRECTORY="$( cd "$SCRIPT_DIRECTORY""$PROGRAM_RELATIVE_DIRECTORY" && pwd )/"

PROGRAM="DataStructures*"
declare -a STRUCTURES=("list" "vector" "set" "custom_list" "custom_tree")
DICTIONARY="dict.dat"
declare -a TEXTS=("kaddath.txt" "kjv.txt" "big.txt")
OUTPUT_DIRECTORY="output/"
OUTPUT_PREFIX="out-"

FAILURE_SUMMARY=""

if [ -z "$1" ] || [ "$1" != "all" ]
then
    declare -a TEXTS=("kaddath.txt")
fi

if [ ! -d "$ABSOLUTE_DIRECTORY""$OUTPUT_DIRECTORY" ]
then
    mkdir "$ABSOLUTE_DIRECTORY""$OUTPUT_DIRECTORY"
fi

for TEXT in "${TEXTS[@]}"
do
    echo "Starting structures for text "$TEXT""
    for STRUCTURE in "${STRUCTURES[@]}"
    do
        STRUCTURE_FLAG="-s "$STRUCTURE""
        DICTIONARY_FLAG="-d "$ABSOLUTE_DIRECTORY""$DICTIONARY""
        TEXT_FLAG="-t "$ABSOLUTE_DIRECTORY""$TEXT""
        OUTPUT_NAME=""$OUTPUT_PREFIX""$STRUCTURE"-"$TEXT""
        OUTPUT_FLAG="-o "$ABSOLUTE_DIRECTORY""$OUTPUT_DIRECTORY""$OUTPUT_NAME""
        FLAGS=""$STRUCTURE_FLAG" "$DICTIONARY_FLAG" "$TEXT_FLAG" "$OUTPUT_FLAG""

        ${ABSOLUTE_DIRECTORY}${PROGRAM} $FLAGS &
    done
    wait
    echo "Completed all structures for text "$TEXT""

    for ((i = 0; i < ${#STRUCTURES[@]}; i++))
    do
        for ((j = i+1; j < ${#STRUCTURES[@]}; j++))
        do
            FILE1=""$ABSOLUTE_DIRECTORY""$OUTPUT_DIRECTORY""$OUTPUT_PREFIX""${STRUCTURES[$i]}"-"$TEXT""
            FILE2=""$ABSOLUTE_DIRECTORY""$OUTPUT_DIRECTORY""$OUTPUT_PREFIX""${STRUCTURES[$j]}"-"$TEXT""
            if [[ $(comm -3 --check-order <(sort "$FILE1") <(sort "$FILE2")) ]]
            then
                echo "Failed "$FILE1" compared with "$FILE2""
                FAILURE_SUMMARY+="\t"$FILE1" compared with "$FILE2"\n"
            fi
        done
    done
    echo "Completed comparison tests for text "$TEXT""
done

echo ""
echo "========================================================="
echo ""
if [ -z "$FAILURE_SUMMARY" ]
then
    echo "Successfully completed execution and passed all tests."
else
    echo "Failed tests:"
    printf "$FAILURE_SUMMARY"
fi
echo ""
echo "========================================================="
